package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;

import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    public GridPane cc;
    public ToggleGroup SexGroup;
    public ImageView avatar;
    public Button btn;
    public ComboBox local;
    public ComboBox blood;
    @FXML
    private DatePicker birthday;
    public RadioButton rbM;
    public RadioButton rbF;
    public RadioButton rbFree;

    public void doSelectdate(ActionEvent actionEvent) {
        System.out.println(birthday.getValue());
    }

    public void doSex(ActionEvent actionEvent) {
        RadioButton radioButton = (RadioButton) actionEvent.getSource();
        System.out.println(radioButton.getText());


    }

    public void dobtn(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("影像檔", "*.png", "*.jpg", "*.gif"));
        File selectedFile = fileChooser.showOpenDialog(cc.getScene().getWindow());

        avatar.setImage(new Image(selectedFile.toURI().toString()));
    }


    @Override
    public void initialize(URL location, ResourceBundle resource) {
        ObservableList<String> item = FXCollections.observableArrayList();
        item.addAll("O", "A", "B", "AB");
        blood.setItems(item);


        ObservableList<String> itemplace = FXCollections.observableArrayList();
        String filename = "C:\\Users\\CSIE-E518\\Desktop\\new 1.txt";
        String line;

        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
            while ((line = bufferedReader.readLine()) != null) {
                itemplace.add(line);

            }

            bufferedReader.close();
            local.setItems(itemplace);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}